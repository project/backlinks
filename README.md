# Backlinks

Extracts links and/or links to nodes from nodes during `hook_node_presave()`.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/backlinks).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/backlinks).


## Table of contents

- Requirements
- Installation
- Configuration


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. Add the linked_url and linked_node fields to the desired nodes.
1. Specify the fields to search for links.
1. Rebuild links
