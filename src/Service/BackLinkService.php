<?php

declare(strict_types=1);

namespace Drupal\backlinks\Service;

use Drupal\backlinks\BacklinksInterface;
use Drupal\backlinks\BacklinksTrustedHostInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * The backlink service.
 */
class BackLinkService implements BacklinksInterface {

  /**
   * The trusted host service.
   *
   * @var \Drupal\backlinks\BacklinksTrustedHostInterface
   */
  protected $trustedHost;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $storage;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The backlink service constructor.
   *
   * @param \Drupal\backlinks\BacklinksTrustedHostInterface $trusted_host
   *   The trusted host service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    BacklinksTrustedHostInterface $trusted_host,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    RendererInterface $renderer,
  ) {
    $this->trustedHost = $trusted_host;
    $this->storage = $entity_type_manager->getStorage('node');
    $this->logger = $logger;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeLinks($field_list, $bundles = []) {
    $backlinks = [];
    $links = $this->getLinks($field_list);
    foreach ($links as $link) {
      $address = $link['uri'];
      $url = $this->getUrl($address);

      if (!$url) {
        continue;
      }

      if ($url->isExternal()) {
        continue;
      }

      $route = $url->getRouteName();
      if ($route != 'entity.node.canonical') {
        continue;
      }

      $parameter = $url->getRouteParameters();
      $nid = $parameter['node'] ?? NULL;
      if (!$nid) {
        continue;
      }
      $node = $this->storage->load($nid);
      $is_bundle = empty($bundles) || in_array($node->getType(), $bundles);
      if (!$is_bundle) {
        continue;
      }

      $backlinks[] = $nid;
    }

    return array_unique($backlinks);
  }

  /**
   * Get the a URL object from a string address.
   *
   * @param string $address
   *   The address.
   *
   * @return \Drupal\Core\Url|null
   *   The URL object.
   */
  protected function getUrl($address): ?Url {
    $parsed = parse_url($address);
    $schema = $parsed['schema'] ?? NULL;

    $is_not_empty_and_not_http = !is_null($schema) && !in_array($schema, ['http', 'https']);
    if ($is_not_empty_and_not_http) {
      return NULL;
    }
    if (empty($parsed['path'])) {
      return NULL;
    }

    $host = $parsed['host'] ?? NULL;
    $url = NULL;
    if ($host) {
      $internal = $this->isInternal($host);
      if ($internal) {
        try {
          $url = Url::fromUserInput($parsed['path']);
        }
        catch (\Exception $e) {
          Error::logException($this->logger, $e);
          return NULL;
        }

        return $url;
      }

      $url = Url::fromUri($address);

      return $url;
    }
    $first_char = substr($address, 0, 1);
    if (!in_array($first_char, ['/', '?', '#'])) {
      return NULL;
    }

    try {
      $url = Url::fromUserInput($address);
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
      return NULL;
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinks($field_list) {
    $hrefs = [];
    foreach ($field_list as $field) {
      $field_data = $field->view('full');
      if (empty($field_data)) {
        continue;
      }

      $markup = $this->renderer->render($field_data);
      if (is_null($markup) || !count($markup)) {
        continue;
      }
      $html = (string) $markup;
      if (empty($html)) {
        continue;
      }

      try {
        $dom = new \DomDocument();
        $dom->loadHTML($html);
        foreach ($dom->getElementsByTagName('a') as $item) {
          $address = $item->getAttribute('href');
          if (empty($address)) {
            continue;
          }
          // Regex for file:// and mailto:.
          if (preg_match('/^(file|mailto|javascript|onenote):/', $address)) {
            continue;
          }
          $title = $item->nodeValue;
          $title = substr($title, 0, 255);
          $hrefs[] = [
            'uri' => $address,
            'title' => $title,
            'options' => [],
          ];
        }
      }
      catch (\Exception $e) {
        Error::logException($this->logger, $e);
        continue;
      }

    }

    return $hrefs;
  }

  /**
   * Check if a host is internal.
   *
   * @param string $host
   *   The host to check.
   *
   * @return bool
   *   Whether the host is internal.
   */
  protected function isInternal($host) {
    return $this->trustedHost->isTrustedHost($host);
  }

}
