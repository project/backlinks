<?php

declare(strict_types=1);

namespace Drupal\backlinks\Service;

use Drupal\backlinks\BacklinksInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Utility\Error;
use Drupal\node\NodeInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The entity link service.
 */
class EntityLinkService {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The link service.
   *
   * @var \Drupal\backlinks\BacklinksInterface
   */
  protected $linkService;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The entity link service constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\backlinks\BacklinksInterface $link_service
   *   The link service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityFieldManagerInterface $entity_field_manager,
    BacklinksInterface $link_service,
    RequestStack $request_stack,
    LoggerInterface $logger,
  ) {

    $this->logger = $logger;
    $this->entityFieldManager = $entity_field_manager;
    $this->linkService = $link_service;
    $this->settings = $config_factory->get('backlinks.settings');
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * Get the linked URL.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return \Drupal\Core\Url[]
   *   The URL object.
   */
  public function getLinkedUrl(NodeInterface $node): array {
    $default_schema = $this->settings->get('schema');
    $default_host = $this->settings->get('host');
    $fields_to_search = $node
      ->type
      ->entity
      ->getThirdPartySetting('backlinks', 'fields') ?? [];
    $url_links = [];
    foreach ($fields_to_search as $field) {
      try {
        $field_list = $node->get($field);
        $links = $this->linkService->getLinks($field_list);
        $url_links = array_merge($url_links, $links);
      }
      catch (\Exception $e) {
        $this->logger->error($node->id());
        Error::logException($this->logger, $e);
      }
    }

    foreach ($url_links as $index => $url) {
      $parsed = parse_url($url['uri']);
      if (isset($parsed['host'])) {
        continue;
      }

      $server_host = $this->request->server->get('HTTP_HOST');
      $host = $default_host ?? $server_host;
      $schema = $parsed['scheme'] ?? $default_schema ?? 'http';
      $path = $parsed['path'] ?? '/node/' . $node->id();
      $query = isset($parsed['query']) ? '?' . $parsed['query'] : '';
      $fragment = isset($parsed['fragment']) ? '#' . $parsed['fragment'] : '';

      $url_links[$index]['uri'] = $schema . '://' . $host . $path . $query . $fragment;
    }

    return $url_links;
  }

  /**
   * Get the linked node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return int[]
   *   The linked node.
   */
  public function getLinkedNode(NodeInterface $node): array {
    $fields = $this->entityFieldManager->getFieldDefinitions('node', $node->getType());
    $target_bundles = $fields['linked_node']->getSetting('handler_settings')['target_bundles'] ?? [];

    $fields_to_search = $node
      ->type
      ->entity
      ->getThirdPartySetting('backlinks', 'fields') ?? [];

    $node_links = [];
    foreach ($fields_to_search as $field) {
      if (!$node->hasField($field)) {
        continue;
      }
      try {
        $field_list = $node->get($field);
        $links = $this->linkService->getNodeLinks($field_list, $target_bundles);
        $node_links = array_merge($node_links, $links);
      }
      catch (\Exception $e) {
        $this->logger->error('There was an error with <a href="@url">@label</a>', [
          '@url' => $node->toUrl(),
          '@label' => $node->label(),
        ]);
        Error::logException($this->logger, $e);
      }
    }

    return $node_links;
  }

}
