<?php

declare(strict_types=1);

namespace Drupal\backlinks\Service;

use Drupal\backlinks\BacklinksTrustedHostInterface;
use Drupal\Core\Site\Settings;

/**
 * The trusted host service.
 */
class TrustedHostService implements BacklinksTrustedHostInterface {

  /**
   * Check if a host is trusted.
   *
   * @param string $host
   *   The host to check.
   *
   * @return bool
   *   Whether the host is trusted.
   */
  public function isTrustedHost($host) {
    $trusted_hosts = Settings::get('trusted_host_patterns', []);
    foreach ($trusted_hosts as $pattern) {
      $reg_ex = '/' . $pattern . '/';
      if (preg_match($reg_ex, $host)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
