<?php

namespace Drupal\backlinks;

/**
 * The backlinks interface.
 */
interface BacklinksInterface {

  /**
   * Get the node links.
   *
   * @param array $field_list
   *   The field list.
   * @param array $bundles
   *   The bundles.
   *
   * @return array
   *   The node links.
   */
  public function getNodeLinks($field_list, $bundles = []);

  /**
   * Get the links from a field list.
   *
   * @param array $field_list
   *   The field list.
   *
   * @return array
   *   The links.
   */
  public function getLinks($field_list);

}
