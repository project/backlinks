<?php

declare(strict_types=1);

namespace Drupal\backlinks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings Form for Open Knowledge Link.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'backlinks.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backlinks_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('backlinks.settings');
    $form['schema'] = [
      '#type' => 'select',
      '#title' => $this->t('Schema'),
      '#description' => $this->t('The schema to use when saving to the field (linked_url).'),
      '#options' => [
        'http' => $this->t('http'),
        'https' => $this->t('https'),
      ],
      '#default_value' => $settings->get('schema'),
    ];
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('The hostname to use when saving to the field (linked_url).'),
      '#default_value' => $settings->get('host'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('backlinks.settings');
    $settings
      ->set('schema', $form_state->getValue('schema'))
      ->set('host', $form_state->getValue('host'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
