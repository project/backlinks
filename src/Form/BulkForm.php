<?php

namespace Drupal\backlinks\Form;

use Drupal\backlinks\BacklinksTrustedHostInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The bulk form.
 */
class BulkForm extends FormBase {

  /**
   * The trusted host service.
   *
   * @var \Drupal\backlinks\BacklinksTrustedHostInterface
   */
  protected $trustedHost;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bulk form constructor.
   *
   * @param \Drupal\backlinks\BacklinksTrustedHostInterface $trusted_host
   *   The trusted host service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    BacklinksTrustedHostInterface $trusted_host,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    $this->trustedHost = $trusted_host;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('backlinks.trusted_hosts'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backlinks_bulk_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node_types = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $node_type) {
      $node_types[$node_type->id()] = $node_type->label();
    }
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node types'),
      '#description' => $this->t('Select the node types to check for backlinks.'),
      '#required' => TRUE,
      '#options' => $node_types,
    ];
    $form['update'] = [
      '#type' => 'radios',
      '#title' => $this->t('Update'),
      '#options' => [
        'database' => $this->t('Database'),
        'entity' => $this->t('Save Nodes'),
      ],
      '#description' =>
      $this->t('Ony save nodes if new revisions are acceptable.'),
      '#default_value' => 'database',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $bundles = $form_state->getValue('bundles');
    $update = $form_state->getValue('update');

    $batch = [
      'title' => $this->t('Checking backlinks'),
      'operations' => [
        [
          [BulkForm::class, 'buildLinks'],
          [$bundles, $update],
        ],
      ],
    ];

    batch_set($batch);

  }

  /**
   * Build the links.
   */
  public static function buildLinks($node_types, $update, &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $node_query = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->getQuery();
      $nids = $node_query
        ->condition('type', $node_types, 'IN')
        ->accessCheck(FALSE)
        ->execute();

      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($nids);
      $context['sandbox']['nids'] = $nids;
    }

    $nid = array_pop($context['sandbox']['nids']);
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->load($nid);
    $database = \Drupal::database();

    if ($update == 'database') {
      $entity_link_service = \Drupal::service('backlinks.entity');
      $has_linked_url = $node->hasField('linked_url');
      $has_linked_node = $node->hasField('linked_node');

      if ($has_linked_node) {
        $node_links = $entity_link_service->getLinkedNode($node);
        self::updateNodeReferences($database, $node, $node_links);
      }

      if ($has_linked_url) {
        $url_links = $entity_link_service->getLinkedUrl($node);
        self::updateUrlReferences($database, $node, $url_links);
      }

    }
    elseif ($update == 'entity') {
      $node->save();
    }

    $context['sandbox']['current_node'] = $node->nid;
    $context['message'] = t('Now processing @label', ['@label' => $node->label()]);

    $context['sandbox']['progress'] += 1;
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }

    if (empty($context['sandbox']['nids'])) {
      $context['finished'] = 1;
    }

    Cache::invalidateTags(['node:' . $nid]);
  }

  /**
   * Update the node references.
   */
  public static function updateNodeReferences($database, $node, $node_links) {
    $nid = $node->id();
    $revision_id = $node->getRevisionId();
    $bundle = $node->bundle();

    $database->delete('node__linked_node')
      ->condition('entity_id', $nid)
      ->execute();

    if (empty($node_links)) {
      return;
    }
    $query = $database->insert('node__linked_node')
      ->fields([
        'bundle',
        'entity_id',
        'revision_id',
        'langcode',
        'linked_node_target_id',
        'delta',
      ]);

    foreach ($node_links as $index => $target_id) {
      $query->values([
        'bundle' => $bundle,
        'entity_id' => $nid,
        'revision_id' => $revision_id,
        'langcode' => $node->language()->getId(),
        'linked_node_target_id' => $target_id,
        'delta' => $index,
      ]);
    }

    $query->execute();
  }

  /**
   * Update the URL references.
   */
  public static function updateUrlReferences($database, $node, $url_links) {
    $nid = $node->id();
    $revision_id = $node->getRevisionId();
    $bundle = $node->bundle();

    $database->delete('node__linked_url')
      ->condition('entity_id', $nid)
      ->execute();

    if (empty($url_links)) {
      return;
    }
    $query = $database->insert('node__linked_url')
      ->fields([
        'bundle',
        'entity_id',
        'revision_id',
        'langcode',
        'linked_url_uri',
        'linked_url_title',
        'linked_url_options',
        'delta',
      ]);

    foreach ($url_links as $index => $url) {
      $query->values([
        'bundle' => $bundle,
        'entity_id' => $nid,
        'revision_id' => $revision_id,
        'langcode' => $node->language()->getId(),
        'linked_url_uri' => $url['uri'],
        'linked_url_title' => $url['title'],
        'linked_url_options' => serialize($url['options']),
        'delta' => $index,
      ]);
    }

    $query->execute();
  }

}
