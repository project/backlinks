<?php

namespace Drupal\backlinks;

/**
 * The trusted host interface.
 */
interface BacklinksTrustedHostInterface {

  /**
   * Check if a host is trusted.
   *
   * @param string $host
   *   The host to check.
   *
   * @return bool
   *   Whether the host is trusted.
   */
  public function isTrustedHost($host);

}
