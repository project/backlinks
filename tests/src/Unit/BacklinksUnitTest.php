<?php

declare(strict_types=1);

namespace Drupal\Tests\backlinks\Unit;

use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

require_once __DIR__ . '/../../../backlinks.module';

/**
 * Tests the BackLinks class.
 *
 * @group backlinks
 */
class BacklinksUnitTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The backlink service.
   *
   * @var \Drupal\backlinks\Service\BackLinkService
   */
  protected $backlinkService;

  /**
   * The entity link service.
   *
   * @var \Drupal\backlinks\Service\EntityLinkService
   */
  protected $entityLinkService;

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $this->backlinkService = $this->createMock('Drupal\backlinks\Service\BackLinkService');
    $container->set('backlinks.links', $this->backlinkService);

    $config_factory = $this->getConfigFactoryStub([
      'knowledge.settings' => [
        'node_types' => ['article'],
      ],
    ]);
    $container->set('config.factory', $config_factory);

    $this->entityLinkService = $this->createMock('Drupal\backlinks\Service\EntityLinkService');
    $container->set('backlinks.entity', $this->entityLinkService);

    \Drupal::setContainer($container);
  }

  /**
   * Test the presave hook.
   *
   * @covers ::backlinks_node_presave
   * @coversFunction ::backlinks_get_node_links
   */
  public function testPresaveHookWithArticle() {

    $node = $this->createMock('Drupal\node\NodeInterface');

    backlinks_node_presave($node);

    $this->assertTrue(TRUE);
  }

  /**
   * Test the presave hook.
   */
  public function testPresaveHookWithPage() {

    $node = $this->createMock('Drupal\node\NodeInterface');

    backlinks_node_presave($node);

    $this->assertTrue(TRUE);
  }

}
