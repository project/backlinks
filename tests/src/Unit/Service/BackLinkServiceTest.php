<?php

declare(strict_types=1);

namespace Drupal\Tests\backlinks\Unit\Service;

use Drupal\backlinks\Service\BackLinkService;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Drupal\Core\Render\Markup;

/**
 * Tests the BackLinkService class.
 */
class BackLinkServiceTest extends UnitTestCase {

  /**
   * The backlink service.
   *
   * @var \Drupal\backlinks\Service\BackLinkService
   */
  protected $service;

  /**
   * The trusted host service.
   *
   * @var \Drupal\backlinks\BacklinksTrustedHostInterface
   */
  protected $trustedHost;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $this->pathValidator = $this->createMock('Drupal\Core\Path\PathValidatorInterface');
    $container->set('path.validator', $this->pathValidator);

    $this->trustedHost = $this->createMock('Drupal\backlinks\BacklinksTrustedHostInterface');
    $container->set('backlinks.trusted_host', $this->trustedHost);

    $this->nodeStorage = $this->createMock('Drupal\node\NodeStorageInterface');

    $entity_type_manager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $container->set('entity_type.manager', $entity_type_manager);

    $this->entityFieldManager = $this->createMock('Drupal\Core\Entity\EntityFieldManagerInterface');
    $container->set('entity_field.manager', $this->entityFieldManager);

    $entity_type_manager->expects($this->once())
      ->method('getStorage')
      ->with('node')
      ->willReturn($this->nodeStorage);

    $this->logger = $this->createMock('Psr\Log\LoggerInterface');
    $container->set('logger.channel.backlinks', $this->logger);

    $this->renderer = $this->createMock('Drupal\Core\Render\RendererInterface');
    $container->set('renderer', $this->renderer);

    \Drupal::setContainer($container);

    $this->service = new BackLinkService($this->trustedHost, $entity_type_manager, $this->logger, $this->renderer);
  }

  /**
   * Test getLinks method.
   */
  public function testGetLinks(): void {
    $html = '<a href="http://example.com">Example</a>
    or <a href="http://example2.com/node/100">Example 2</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];

    $markup = Markup::create($html);

    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);

    $field_a = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field_a->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $links = $this->service->getLinks([$field_a]);

    $this->assertIsArray($links);
    $this->assertCount(2, $links);
    $this->assertArrayHasKey('title', $links[0]);
    $this->assertArrayHasKey('uri', $links[0]);
    $this->assertEquals('Example', $links[0]['title']);
    $this->assertEquals('http://example.com', $links[0]['uri']);
  }

  /**
   * Test getNodeLinks method.
   */
  public function testGetNodeLinks(): void {
    $html = '<a href="/node/101">An article</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);

    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->prophesize('Drupal\Core\Url');
    $url_a->isExternal()
      ->willReturn(FALSE)
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteName()
      ->willReturn('entity.node.canonical')
      ->shouldBeCalledOnce($this->once());
    $url_a->getOptions()
      ->willReturn([])
      ->shouldBeCalledOnce($this->once());
    $url_a->setOptions([])
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteParameters()
      ->willReturn(['node' => 101])
      ->shouldBeCalledOnce($this->once());

    $this->pathValidator->expects($this->once())
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101')
      ->willReturn($url_a->reveal());

    $node = $this->createMock('Drupal\node\NodeInterface');
    $this->nodeStorage->expects($this->once())
      ->method('load')
      ->with(101)
      ->willReturn($node);

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(1, $links);
    $this->assertEquals(101, $links[0]);
  }

  /**
   * Test getNodeLinks method.
   */
  public function testGetNodeLinksOneNoteSchema(): void {
    $html = '<a href="/node/101">An article</a> <a href="onenote://something/something/dark/side">An article</a> ';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);
    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->prophesize('Drupal\Core\Url');
    $url_a->isExternal()
      ->willReturn(FALSE)
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteName()
      ->willReturn('entity.node.canonical')
      ->shouldBeCalledOnce($this->once());
    $url_a->getOptions()
      ->willReturn([])
      ->shouldBeCalledOnce($this->once());
    $url_a->setOptions([])
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteParameters()
      ->willReturn(['node' => 101])
      ->shouldBeCalledOnce($this->once());

    $this->pathValidator->expects($this->once())
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101')
      ->willReturn($url_a->reveal());

    $node = $this->createMock('Drupal\node\NodeInterface');
    $this->nodeStorage->expects($this->once())
      ->method('load')
      ->with(101)
      ->willReturn($node);

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(1, $links);
    $this->assertEquals(101, $links[0]);
  }

  /**
   * Test getNodeLinks method.
   */
  public function testGetNodeLinksDuplicate(): void {
    $html = '<a href="/node/101">An article</a> and a
    <a href="/node/101">Duplicate article</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);
    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->createMock('Drupal\Core\Url');
    $url_a->expects($this->exactly(2))
      ->method('isExternal')
      ->willReturn(FALSE);

    $url_a->expects($this->exactly(2))
      ->method('getRouteName')
      ->willReturn('entity.node.canonical');

    $url_a->expects($this->exactly(2))
      ->method('getOptions')
      ->willReturn([]);

    $url_a->expects($this->exactly(2))
      ->method('setOptions')
      ->with([]);

    $url_a->expects($this->exactly(2))
      ->method('getRouteParameters')
      ->willReturn(['node' => 101]);

    $this->pathValidator->expects($this->exactly(2))
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101')
      ->willReturn($url_a);

    $node = $this->createMock('Drupal\node\NodeInterface');
    $this->nodeStorage->expects($this->exactly(2))
      ->method('load')
      ->with(101)
      ->willReturn($node);

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(1, $links);
    $this->assertEquals(101, $links[0]);
  }

  /**
   * Test getNodeLinks method.
   */
  public function testGetNodeLinksNoBundle(): void {
    $html = '<a href="/node/101">An article</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);

    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->prophesize('Drupal\Core\Url');
    $url_a->isExternal()
      ->willReturn(FALSE)
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteName()
      ->willReturn('entity.node.canonical')
      ->shouldBeCalledOnce($this->once());
    $url_a->getOptions()
      ->willReturn([])
      ->shouldBeCalledOnce($this->once());
    $url_a->setOptions([])
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteParameters()
      ->willReturn(['node' => 101])
      ->shouldBeCalledOnce($this->once());

    $this->pathValidator->expects($this->once())
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101')
      ->willReturn($url_a->reveal());

    $node = $this->createMock('Drupal\node\NodeInterface');
    $node->expects($this->once())
      ->method('getType')
      ->willReturn('page');

    $this->nodeStorage->expects($this->once())
      ->method('load')
      ->with(101)
      ->willReturn($node);

    $links = $this->service->getNodeLinks([$field], ['article']);

    $this->assertIsArray($links);
    $this->assertCount(0, $links);
  }

  /**
   * Test getNodeLinks method.
   */
  public function testGetNodeLinksInvalidRouteParameter(): void {
    $html = '<a href="/node/101">An article</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);
    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->prophesize('Drupal\Core\Url');
    $url_a->isExternal()
      ->willReturn(FALSE)
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteName()
      ->willReturn('entity.node.canonical')
      ->shouldBeCalledOnce($this->once());
    $url_a->getOptions()
      ->willReturn([])
      ->shouldBeCalledOnce($this->once());
    $url_a->setOptions([])
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteParameters()
      ->willReturn(['node' => NULL])
      ->shouldBeCalledOnce($this->once());

    $this->pathValidator->expects($this->once())
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101')
      ->willReturn($url_a->reveal());

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(0, $links);
  }

  /**
   * Test getNodeLinks method.
   */
  public function testGetNodeLinksNonCanonical(): void {
    $html = '<a href="/node/101/edit">An article</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);
    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->prophesize('Drupal\Core\Url');
    $url_a->isExternal()
      ->willReturn(FALSE)
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteName()
      ->willReturn('entity.node.edit_form')
      ->shouldBeCalledOnce($this->once());
    $url_a->getOptions()
      ->willReturn([])
      ->shouldBeCalledOnce($this->once());
    $url_a->setOptions([])
      ->shouldBeCalledOnce($this->once());

    $this->pathValidator->expects($this->once())
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101/edit')
      ->willReturn($url_a->reveal());

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(0, $links);
  }

  /**
   * Test getNodeLinks method with full qualified domain name.
   */
  public function testGetNodeLinksFullQualifiedDomainName(): void {
    $html = '<a href="http://example.com/node/101">An article</a>';
    $render_array = [
      "#type" => "processed_text",
      "#text" => $html,
      "#format" => "basic_html",
      "#langcode" => "en",
    ];
    $markup = Markup::create($html);
    $this->renderer->expects($this->exactly(1))
      ->method('render')
      ->with($render_array)
      ->willReturn($markup);
    $this->trustedHost->expects($this->once())
      ->method('isTrustedHost')
      ->with('example.com')
      ->willReturn(TRUE);

    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($render_array);

    $url_a = $this->prophesize('Drupal\Core\Url');
    $url_a->isExternal()
      ->willReturn(FALSE)
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteName()
      ->willReturn('entity.node.canonical')
      ->shouldBeCalledOnce($this->once());
    $url_a->getOptions()
      ->willReturn([])
      ->shouldBeCalledOnce($this->once());
    $url_a->setOptions([])
      ->shouldBeCalledOnce($this->once());
    $url_a->getRouteParameters()
      ->willReturn(['node' => 101])
      ->shouldBeCalledOnce($this->once());

    $this->pathValidator->expects($this->once())
      ->method('getUrlIfValidWithoutAccessCheck')
      ->with('node/101')
      ->willReturn($url_a->reveal());

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(1, $links);
    $this->assertEquals(101, $links[0]);
  }

  /**
   * Test getNodeLinks method with external URL.
   */
  public function testGetNodeLinksExternalUrl(): void {
    $html = '<a href="https://binaryalchemist.com">Some external site</a>';
    $markup = Markup::create($html);
    $field = $this->createMock('Drupal\Core\Field\FieldItemInterface');
    $field->expects($this->once())
      ->method('view')
      ->with('full')
      ->willReturn($markup);

    $links = $this->service->getNodeLinks([$field]);

    $this->assertIsArray($links);
    $this->assertCount(0, $links);
  }

}
