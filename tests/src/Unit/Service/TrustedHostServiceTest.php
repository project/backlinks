<?php

namespace Drupal\Tests\backlinks\Unit\Service;

use Drupal\backlinks\Service\TrustedHostService;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the TrustedHostService class.
 *
 * @group backlinks
 * @coversDefaultClass \Drupal\backlinks\Service\TrustedHostService
 */
class TrustedHostServiceTest extends UnitTestCase {

  /**
   * The trusted host service.
   *
   * @var \Drupal\backlinks\Service\TrustedHostService
   */
  protected $trustedHostService;

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setUp();

    $this->trustedHostService = new TrustedHostService();
  }

  /**
   * Test the isTrustedHost method.
   */
  public function testIsTrustedHost() {
    $is_trusted = $this->trustedHostService->isTrustedHost('example.com');
    $this->assertFalse($is_trusted);
  }

}
